import matplotlib.pyplot as plt
from models import Lambda4OrientationDataSet as lo

lo_data_set = lo.Lambda4OrientationDataSet('data/data.csv')
jones_vector, dia_offset = lo_data_set.fit()
scale = 1.5
fig4, axs4 = plt.subplots(2, 1, constrained_layout=True, figsize=(6.4 / scale, 4.8 / scale))
jones_vector.plot_ellipsis(fig4, axs4[1])
print(jones_vector)
print(f'ellipticity{jones_vector.ellipticity}')
print(f'alpha{jones_vector.alpha}')
print(f'intensity{jones_vector.intensity}')
print(f'delta{jones_vector.delta}')
lo_data_set.plot_jones_fit(jones_vector, dia_offset, fig4, axs4[0])

fig4.savefig('results/3_2_graph_polarimeter.pdf')
fig4.show()